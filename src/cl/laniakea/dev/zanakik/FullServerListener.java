package cl.laniakea.dev.zanakik;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class FullServerListener implements Listener {
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		int maxplayers = Bukkit.getServer().getMaxPlayers();
		int onlineplayers = Bukkit.getServer().getOnlinePlayers().size();
		if (onlineplayers < maxplayers) {
			return;
		}
		
		Bukkit.broadcastMessage("¡Servidor lleno! procederá un zanakik");
		Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "zkik");
	}
}
