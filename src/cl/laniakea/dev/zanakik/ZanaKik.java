package cl.laniakea.dev.zanakik;
import org.bukkit.plugin.java.JavaPlugin;

public class ZanaKik extends JavaPlugin{
	@Override
    public void onEnable() {		
		this.getCommand("zkik").setExecutor(new CommandZkik(this));
		
		getServer().getPluginManager().registerEvents(new FullServerListener(), this);
    }
    
    @Override
    public void onDisable() {
    	
    }
}
