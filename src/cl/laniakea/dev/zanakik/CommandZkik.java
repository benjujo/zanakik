package cl.laniakea.dev.zanakik;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.bukkit.BanList.Type;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CommandZkik implements CommandExecutor {
	private ZanaKik plugin;

	public CommandZkik(ZanaKik zanaKik) {
		this.plugin = zanaKik;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		Player[] players = getKickablePlayers(Bukkit.getOnlinePlayers().toArray(new Player[0]));
		
		if(players.length < 1) {
			return false;
		}
		Random rnd = new Random();
		int rndindex = rnd.nextInt(players.length);
		Player rndplayer = (Player) players[rndindex];
		kik(rndplayer);
		return true;
	}
	
	private Player[] getKickablePlayers(Player[] onlineplayers) {
		ArrayList<Player> playerList = new ArrayList<Player>();
		for(Player p: onlineplayers) {
			if(!isDonator(p)) {
				playerList.add(p);
			}
		}
		Player[] kickableplayers = playerList.toArray(new Player[playerList.size()]);
		return kickableplayers;
	}
	
	public void dkik(Player player) {
		System.out.println(player.getName() + " sería zanakikeado.");
	}
	
	private boolean isDonator(Player player) {
		return player.hasPermission("group.donator");
	}

	public void kik(Player player) {
		ItemStack carrot = new ItemStack(Material.CARROT);
		
		ItemMeta meta = carrot.getItemMeta();
		meta.setDisplayName("zanakik");
		List<String> loresList = new ArrayList<String>();
		loresList.add("Fuiste seleccionadx aleatoriamente");
		loresList.add("para ser kickeadx.");
		loresList.add("Nada personal...");
		loresList.add("Guarda este ítem para poder canjearlo.");
		loresList.add("Ve a §4/warp dinero §5para más info.");
		meta.setLore(loresList);
		carrot.setItemMeta(meta);
		
		player.getInventory().addItem(carrot);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.SECOND, 30);
		Date expires = cal.getTime();
		
		Bukkit.getBanList(Type.NAME).addBan(player.getName(), "Zanakik", expires, "zanakik");
		Bukkit.broadcastMessage(player.getName() + " será zanakikeadx.");
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				player.kickPlayer("Zanakik");
			}
		}, 300L);
		
	}

}
